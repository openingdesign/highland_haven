Another name for a pocket neighborhood is a micro-neighborhood. Other terms used to describe this type of neighborhood include cottage clusters, small-scale neighborhoods, and village-style developments. The idea behind a pocket neighborhood is to create a small, close-knit community that encourages social interaction and a sense of belonging among its residents. These neighborhoods often feature compact, energy-efficient homes clustered around shared spaces such as gardens, courtyards, or community buildings.


- storm water mitigation?
	- retention pond
- Process of annexation?
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTc4NjUyNDA1NSwtMTIyNDU1MjgzNV19
-->